package com.example.discussion.services;

import com.example.discussion.config.JwtToken;
import com.example.discussion.models.Post;
import com.example.discussion.models.User;
import com.example.discussion.repositories.PostRepository;
import com.example.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
//The @Service annotation will allow us to use the CRUD methods inherited from the CRUDRepository even though interfaces do not contain implementation/method bodies
public class PostServiceImpl implements PostService{

    //@Autowired indicates that Spring should automatically inject an instance of the POstRepository into the postRepository
    @Autowired

    //"postRepository" is a variable that represents the instance of PostRepository
    private PostRepository postRepository;

    @Autowired
    // "userRepository" is a variable that represents the instance of UserRepository
    private UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    //Create post
    public void createPost(String stringToken, Post post) {
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
    }

    //Get posts
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    //Delete post

    public ResponseEntity deletePost(Long id, String stringToken) {
        Post postForDeleting = postRepository.findById(id).get();
        String postAuthorName = postForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)){
            postRepository.deleteById(id);
            return new ResponseEntity<>("Post deleted successfully!", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    //Update post
    public ResponseEntity updatePost(Long id, String stringToken, Post post) {
        Post postForUpdating = postRepository.findById(id).get();
        String postAuthorName = postForUpdating.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUserName.equals(postAuthorName)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());
            postRepository.save(postForUpdating);
            return new ResponseEntity<>("Post updated successfully!", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this post.", HttpStatus.UNAUTHORIZED);
        }
    }


    //Get user posts

    public Iterable<Post> getMyPosts(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getPosts();
    }
}
